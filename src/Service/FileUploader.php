<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\Filesystem\Filesystem;

class FileUploader
{
    private SluggerInterface $sluger;

    private string $uploadsDirectory;

    private Filesystem $filesystem;

    public function __construct(SluggerInterface $sluger, string $uploadsDirectory, Filesystem $filesystem)
    {
        $this->sluger = $sluger;
        $this->uploadsDirectory = $uploadsDirectory;
        $this->filesystem = $filesystem;
    }

    /**
     * Upload a file and return it s filaname and filepath
     *
     * @param  UploadedFile $file     *
     * @return array{'fileName': string, 'filePath': string}
     * @author Hamza
     * @version 1.0
     */
    public function upload(UploadedFile $file): array
    {
        // recuperer le nom de file
            $fileName = $this->generateUniqFileame($file);
        // move this file to public/uploads
        try{
         if(!$file->getRealPath() || ! $this->uploadsDirectory . $fileName){
            //  throw new FileException("path de image incorrecte ou introuvable");
         }
            $this->filesystem->copy($file->getRealPath(), $this->uploadsDirectory . $fileName);
            // $file->move($this->uploadsDirectory, $fileName);
        }catch(FileException $fileException){
            throw $fileException;
        }
        return [
            'fileName' => $fileName,
            'filePath' => $this->uploadsDirectory . $fileName
        ];
    }

    /**
     * Undocumented function
     * @param  \Symfony\Component\HttpFoundation\File\UploadedFile $file
     *
     * @return string
     * @author Hamza
     * @version 1.0
     */
    private function generateUniqFileame(UploadedFile $file): string
    {
        $origineFileName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);

        $originalFileNameSlugged = $this->sluger->slug($origineFileName);

        $randomID = uniqid();

        return "{$originalFileNameSlugged}-{$randomID}-{$file->guessExtension()} ";


      }

}
