<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class, [
                'label'         => "Email",
                'required'    => true,
                'attr'           => [
                         'autofocus' => true
                        ]
            ])
            ->add('password', RepeatedType::class, [
                'type'                      => PasswordType::class,
                'invalid_message' => "Les mots de passe saisie ne correspond pas !!",
                'required'               => true,
                'first_options'        => [
                    'label'           => "Mot de passe ",
                    'label_attr'    => [
                                        'title'         => 'Pour de raison de securité le mot de passe ne doit contenir moin            de 5 carectere alphnumlereque y compris majuscule etc'
                                    ],
                    'attr'              => [
                                        'pattern'       => "^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{5,}$",
                                        'title'           =>  'Pour de raison de securité le mot de passe ne doit contenir que de carectere',
                                        'maxlength'  => 255
                                    ]
                ],
                'second_options'    => [
                                            'label'           => "Confirmer votre mot de passe ",
                                            'label_attr'    => [
                                                                 'title' => 'Confirmer votre mot de passe'
                                            ],
                                            'attr'          => [
                                                'pattern'       => "^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{5,}$",
                                                'title'     =>  'Pour de raison de securité le mot de passe ne doit contenir que de carectere',
                                                'maxlength'     => 255
                                            ]
                ],
            ])
            ->add('agreeTerms', CheckboxType::class, [
                'label'     =>  'J\'accepte les conditions d\'utilisation de ce site pour vous inscrire!',
                 'required'                  => true,
                'mapped' => false,
                'constraints' => [
                    new IsTrue([
                        'message' => 'J\'accepte les conditions d\'utilisation de ce site pour vous inscrire!',
                    ]),
                ],
            ])
            // ->add('plainPassword', PasswordType::class, [
            //     // instead of being set onto the object directly,
            //     // this is read and encoded in the controller
            //     'mapped' => false,
            //     'attr' => ['autocomplete' => 'new-password'],
            //     'constraints' => [
            //         new NotBlank([
            //             'message' => 'Please enter a password',
            //         ]),
            //         new Length([
            //             'min' => 6,
            //             'minMessage' => 'Your password should be at least {{ limit }} characters',
            //             // max length allowed by Symfony for security reasons
            //             'max' => 4096,
            //         ]),
            //     ],
            // ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
