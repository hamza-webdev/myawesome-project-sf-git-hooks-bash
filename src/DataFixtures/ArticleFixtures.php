<?php

namespace App\DataFixtures;

use Faker;
use DateTime;
use DateTimeImmutable;
use App\Entity\Article;
use App\Entity\Author;
use App\Entity\Category;
use App\Entity\Picture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\String\Slugger\SluggerInterface;

class ArticleFixtures extends Fixture implements DependentFixtureInterface
{
    protected ObjectManager $manager;
    protected SluggerInterface $slug;

    public function __construct(SluggerInterface $slugger)
    {
        $this->slug = $slugger;
    }

    /**
     * lLoad fixtures
     *
     * @param  \Doctrine\Persistence\ObjectManager $manager
     *
     * @return void
     * @author Hamza
     * @version 1.0
     */
    public function load(ObjectManager $manager): void
    {

        $this->manager = $manager;
        $this->generateArticle(6);

        $this->manager->flush();
    }

    /**
     * [generateArticle description]
     *
     * @param   int   $number  [$number insertion in datatable]
     *
     * @return  void           [rnon retourn]
     */
    private function generateArticle(int $number): void
    {
        for($i=0; $i < $number; $i++){
            $faker =  Faker\Factory::create();
                $article = new Article();
                $dateArray = ($this->generateRandomDateBetweenRange("01/01/2021", "11/11/2021"));
                $title = $faker->sentence();
                $dateString = $dateArray['dateString'];
                $date = $dateArray['dateObject'];
                $slug = $this->slug->slug(strtolower($title) ).'-'.$dateString;
                $picture = $this->getReference("picture{$i}");
                $author = (new Author());
                $author->setName("hamza{$i}");

                $category = (new Category());
            $category->setName("Categorie N°" . $i);

                $article->setTitle($title)
                    ->setContent($faker->paragraph(4))
                    ->setSlug($slug)
                    ->setCreatedAt($date)
                    ->setIsPublished(false)
                    ->setAuthor($author)
                    ->addCategory($category)
                    ->setPicture($picture)
                    ;

                $this->manager->persist($article);
                $this->manager->persist($author);
                $this->manager->persist($category);
                $this->addReference("article{$i}", $article);
                $picture->setArticle($article);
        }
    }

        public function getDependencies()
        {
               return [
                    PictureFixtures::class,
                    CategoryFixtures::class,
                    // AuthorFixtures::class
                ];

        }

    /**
     * Generate Random Date Between Range
     * @param string $start
     * @param string $end
     *
     * @return array{"dateObject": \DateTimeImmutable, "dateString": string}
     * @author\Faker\Factory
     * @version 1.0
     */
        private function generateRandomDateBetweenRange(string $start, string $end): array
        {
            if( !(DateTime::createFromFormat('d/m/Y', $start)) || !(DateTime::createFromFormat('d/m/Y', $end))){
                throw new HttpException(400, "La date saisie doit etre sous la forme d/m/Y");
            }
            //  example of $st\Faker\Factoryart= 12/09/2002 convertir la date en timestamps
            $startDateTimeStamp = (DateTime::createFromFormat('d/m/Y', $start))->getTimestamp();
            $endDateTimeStamp = (DateTime::createFromFormat('d/m/Y', $end))->getTimestamp();

            $randomTimestamp = mt_rand($startDateTimeStamp, $endDateTimeStamp);

            $dateTimeImmutable = (new DateTimeImmutable())->setTimestamp($randomTimestamp);

            // return tableau with 2 entity - 1 object dattimeimmutable 2string date for slug
            return [
                'dateObject' => $dateTimeImmutable,
                'dateString' => $dateTimeImmutable->format('d-m-Y')
            ];
        }
}
