<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class DeleteAndCreateDBWithStrictureWithDataCommand extends Command
{
    protected static $defaultName = 'app:clean-db';
    protected static $defaultDescription = 'Delete And Create DB With Stricture With Data Command';

    protected function configure(): void
    {
        // $this
        //     ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
        //     ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        // ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $io->section("---- Suppression de DB  ---- 😜😜");

        $this->runSymfonyCommand($input, $output, 'cache:clear');

        $this->runSymfonyCommand($input, $output, 'doctrine:database:drop', true);
        $io->section("---- Creation de la base de donnes avec  structure pre-remplis ---- 😜😜");

        $this->runSymfonyCommand($input, $output, 'doctrine:database:create');

  $io->section("\xE2\x9C\x85 ---- CreationDossier HAMZA---- \xE2\x9C\x8A ✌");
//         $this->runSymfonyCommand($input, $output, 'mkdir hamzaHH');

        $io->section("- \xE2\x9C\x85 --- Creation migrations cad les tables---- 😜😜");

        // $this->runSymfonyCommand($input, $output, 'make:migration');
        $this->runSymfonyCommand($input, $output, 'doctrine:migrations:migrate');



            $io->section("----Remplir les tables avec les fixture data fake---- 😜😜");
        $this->runSymfonyCommand($input, $output, 'doctrine:fixtures:load');

        $io->success('  \xE2\x9C\x85   RAS => Votre Base de donnes est propres y remplie avec data de fixtures !😃😃.');
        // $arg1 = $input->getArgument('arg1');

        // if ($arg1) {
        //     $io->note(sprintf('You passed an argument: %s', $arg1));
        // }

        // if ($input->getOption('option1')) {
        //     // ...
        // }



        return Command::SUCCESS;
    }

    private function runSymfonyCommand(
                                                        InputInterface $input,
                                                        OutputInterface $output,
                                                        string $command,
                                                        bool $forceOption = false
                                                        ): void
    {
            $application = $this->getApplication();

            if(!$application){ throw new \LogicException("No application 😖 ");  }

            $command = $application->find($command);
            if($forceOption){
                $input = new ArrayInput([
                    '--force' => true
                ]);
            }

            // mettre la question des input fameux y/O par default a false
            $input->setInteractive(false);

            $command->run($input, $output);
    }
}
