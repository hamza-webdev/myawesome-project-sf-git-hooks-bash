<?php

namespace App\DataFixtures;

use App\Entity\Picture;
use App\Service\FileUploader;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpKernel\KernelInterface;

class PictureFixtures extends Fixture
{

   /** @var array <string>*/
    private static array $pictures = [
        "1.jpeg",
        "2.jpeg",
        "3.jpeg",
        "4.jpeg",
        "5.jpeg",
        "6.jpeg"
    ];

    private string $fileToUploadDirectory;
    private FileUploader $fileUploader;

    private ObjectManager $manager;

    public function __construct(FileUploader $fileUploader, KernelInterface $kernel){

        $this->fileToUploadDirectory = "{$kernel->getProjectDir()}/public/to-upload/";
        $this->fileUploader = $fileUploader;
    }


    public function load(ObjectManager $manager): void
    {

        $this->manager = $manager;
        $this->generateArticlePicture();


        $this->manager->flush();
    }

    private function generateArticlePicture():void
    {

        foreach(self::$pictures as $key => $pictureFile)
        {
            $picture = new Picture();

            $result = $this->fileUploader->upload(new UploadedFile($this->fileToUploadDirectory . $pictureFile,  $pictureFile,  null, null, true) );

            $picture->setPictureName($result['fileName'])
                        ->setPicturePath($result['filePath'])
                        ;

            $this->addReference("picture{$key}", $picture);

            $this->manager->persist($picture);

            // delete dossier to-upload a la dernier iteration du array
            // if($key === array_key_last(self::$pictures)){
                // rmdir ($this->fileToUploadDirectory);
            // }
        }

        $this->manager->flush();

    }


}
