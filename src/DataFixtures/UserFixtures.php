<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\User;
use App\Entity\Author;
use Doctrine\ORM\Query\Expr\Func;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Symfony\Component\PasswordHasher\PasswordHasherInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{

    private ObjectManager $manager;

    private \Faker\Generator $faker;

    private UserPasswordEncoderInterface $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
            $this->passwordEncoder = $passwordEncoder;
    }
    public function load(ObjectManager $manager): void
    {
        $this->faker = Factory::create();


        $this->manager = $manager;
        // $product = new Product();
        // $manager->persist($product);

        $this->generateUsers(2);
        $this->manager->flush();
    }

    public function generateUsers(int $number): void
    {
        $isVerified = [true, false];
        $authorArray = array();



        for($i=0; $i < $number; $i++){
            $user = new User();


                    $author = (new Author())->setName("hamza{$i}");

                    $this->addReference("author".$i, $author);
                    $this->manager->persist($author);


            $user->setEmail($this->faker->email)
                ->setPassword($this->passwordEncoder->encodePassword($user, "test"))
                ->setIsVerified($isVerified[$i])
                ->setAuthor($author);
                ;

                $this->manager->persist($user);
        }

    }
}
