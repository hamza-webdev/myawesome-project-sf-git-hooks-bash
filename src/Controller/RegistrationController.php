<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use App\Service\SendEmail;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

class RegistrationController extends AbstractController
{
    /**
     * @Route("/register", name="app_register")
     */
    public function register(
                                        Request $request,
                                        UserPasswordHasherInterface $userPasswordHasher,
                                        EntityManagerInterface $entityManager,
                                        TokenGeneratorInterface $token,
                                        SendEmail $sendEmail
            ): Response
    {
        $user = new User();

        $form = $this->createForm(RegistrationFormType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $registrationToken = $token->generateToken();
            // encode the plain password
            $user->setPassword( $userPasswordHasher->hashPassword($user,  $form->get('password')->getData() ) )
                    ->setRegistrationToken($registrationToken)
                    ;
            $entityManager->persist($user);
            $entityManager->flush();

            // envoi d'email
            $sendEmail->send([
                'recipient_email'   => $user->getEmail(),
                'subject'               => "Veroification de votre Email pour activer votre compte utilisateur",
                'html_template'   => "registration/register_confirmation_email_html.twig",
                'context'               =>  [
                    'userID'    =>  $user->getId(),
                    'registrationToken'     =>  $registrationToken,
                    'tokenLifeTime'         =>  $user->getAccountMustBeVerifiedBefore()->format('d/m/Y H:i')
                ]
            ]);
            // do anything else you need here, like send an email
            $this->addFlash('success', "Votre compte Utilisateur a bien été créé, Vérifiez votre e-mail pour l'activer.");
            return $this->redirectToRoute('app_login');
        }

        return $this->render('registration/register.html.twig', [ 'registrationForm' => $form->createView() ] );

    }

    /**
     * @Route("/{id<\d+>}/{token}", name="app_verify_account", methods={"GET"})
     */
    public function verifyUserAccount(EntityManagerInterface $entityManager, User $user, string $token,Request $request)
    {
            if(($user->getRegisteredAt() === null) || ($user->getRegistrationToken() !== $token) ||
            ($this->isNotRequestedInTime($user->getAccountMustBeVerifiedBefore())) )
            {
                    throw new AccessDeniedException();
            }

            $user->setIsVerified(true);

            $user->setAccountVerifiedAt(new DateTimeImmutable('now'));

            $user->setRegistrationToken(null);

            $entityManager->flush();
            $this->addFlash('success', 'Votre compte est acctiver, vous pouvez vous connecter');

            return $this->redirectToRoute('app_login');


    }

    private function isNotRequestedInTime(\DateTimeImmutable $accountMustVerifiedBefore): bool
    {
            return (new \DateTimeImmutable('now') > $accountMustVerifiedBefore);
    }



}
