<?php

namespace App\DataFixtures;

use App\Entity\Author;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class AuthorFixtures extends Fixture
{

    private \Faker\Generator $faker;

    private ObjectManager $manager;

    public function load(ObjectManager $manager): void
    {
          $this->manager = $manager;

        $this->faker = Factory::create();
        // $product = new Product();
        // $manager->persist($product);

         $this->generateAuthors(2);

        $this->manager->flush();
    }

     private function generateAuthors(int $number): void
    {
        for($i=0; $i > $number; $i++){
            $author = (new Author())->setName("hamza{$i}")
                                                    ->addArticle($this->getReference("article".mt_rand(1, 6)));

            $this->addReference("author{$i}", $author);

            $this->manager->persist($author);

        }
    }

    public function getDependencies()
        {
               return [
                    ArticleFixtures::class
                ];

        }


}
